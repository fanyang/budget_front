import MarkDown from 'markdown-it'

import hljs from 'highlight.js'

export const mk_render_html = (content) => {
  let md = MarkDown({
      html: true,
      linkify: true,
      typographer: true,
      highlight: function (str, lang) {
          if (lang && hljs.getLanguage(lang)) {
            try {
              return hljs.highlight(lang, str).value;
            } catch (__) {}
          }
          return ''; 
      }
  });
  return md.render(content);
}

export const cal_day = (end, head_block_num) => {
  let mg = (end - head_block_num) * 3;
  if(mg < 0) return false;
  let time_key = {
    day: -1,
    hour: -1,
    min: -1,
    second: -1
  }
  if(mg > 0){
    let days = mg / 24 / 3600;
    if(days > 1){
      time_key.day = parseInt(days);
      return time_key;
    }
    let hours = mg / 3600;
    if(hours > 1){
      time_key.hour = parseInt(hours);
      return time_key;
    }
    let mins = mg / 60;
    if(mins > 1){
      time_key.min = parseInt(mins);
      return time_key;
    }
  }
}

export const wait_time = (second) => {
  return new Promise((rs, rj) => {
    setTimeout(() => {
      rs();
    }, second * 1000);
  });
}


export const split_long_num = (num) => {
  if(num === null) return '';
  let num_str = num + '';
  let [int_num, float_num = ''] = num_str.split('.');
  let num_arr = int_num.split('');
  let n = 0, n_arr = []; 
  for(let i of num_arr.reverse()){
      if(!(n%3) && n){n_arr.push(',');}
      n_arr.push(i);
      n ++;
  };
  return n_arr.reverse().join('') + (float_num ? '.' + float_num : '');
}